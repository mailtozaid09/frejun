const Colors = {
    RED: '#d04848',
    BLACK: '#212227',
    WHITE: '#FFFFFF',
    GRAY: '#C4C4C4',
    YELLOW: '#FBB040',
    PURPLE: '#6D5EF6',
    CREAM: '#fcd5aa',
    ORANGE: '#f26122',
    BLUE: '#015DD3',
    DARK_GRAY: '#666666',
    LIGHT_BLUE: '#679EE5',
    LIGHT_BLUE2: '#F2F7FD',
    LIGHT_GRAY: '#d3d3d4',
    LIGHT_ORANGE: '#fdd9cb',
};

export default Colors;