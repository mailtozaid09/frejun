export const icons = {
    
    cancel: require('../../assets/cancel.png'),
    search: require('../../assets/search.png'),
    seemore: require('../../assets/seemore.png'),

    homeActive: require('../../assets/homeActive.png'),
    home: require('../../assets/home.png'),
    filterActive: require('../../assets/filterActive.png'),
    filter: require('../../assets/filter.png'),
    bridgeActive: require('../../assets/bridgeActive.png'),
    bridge: require('../../assets/bridge.png'),

};