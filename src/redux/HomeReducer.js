import {
    SET_API_DATA,
} from './HomeActionTypes';
  
  const initialState = {
    apiDetails: []
  };
  
  export default HomeReducer = (state = initialState, action) => {
    switch (action.type) {
      case SET_API_DATA:
        return {
          ...state,
          apiDetails: action.payload,
        };
      default:
        return state;
    }
  };
  