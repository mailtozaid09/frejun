import {createStore, combineReducers} from 'redux';
import HomeReducer from './HomeReducer';

const rootReducer = combineReducers({
  HomeReducer: HomeReducer,
});

const Store = createStore(rootReducer);

export default Store;
