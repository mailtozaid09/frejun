import {
    SET_API_DATA,
  } from './HomeActionTypes';
  
  export const setApiData = param => {
    return {
      type: SET_API_DATA,
      payload: param,
    };
  };