import React, {useState, useEffect} from 'react'
import { Text, View, StyleSheet, TouchableOpacity, TextInput, Image, Dimensions,  } from 'react-native'

import Colors from '../../utils/Colors'
import { icons } from '../../utils/Icons';

const SearchBar = (props) => {

    const { value, onChange, clearData } = props;

    return (
        <View style={styles.container} >
                <View style={styles.inputContainer} >
                    <Image
                        source={icons.search}
                        style={styles.searchIcon}
                    />
                    <TextInput 
                        placeholder='Search'
                        value={value}
                        onChangeText={(text) => onChange(text)}
                        style={styles.input}
                    />
                </View>
                {value && (
                    <TouchableOpacity onPress={clearData} style={styles.clearContainer}>
                    <Image
                        source={icons.cancel}
                        style={styles.cancelIcon}
                    />
                </TouchableOpacity>
                )
                }
                
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        height: 50, 
        flexDirection: 'row', 
        alignItems: 'center',
        justifyContent: 'space-between', 
        margin: 15,
        borderWidth: 1,
        borderRadius: 10,
    },
    inputContainer: {
        flex: 1,
        paddingHorizontal: 10,
        flexDirection: 'row', 
        alignItems: 'center',
    },
    input: {
        flex: 1,
        fontSize: 18,
    },
    searchIcon: {
        height: 30,
        width: 30
    },
    cancelIcon: {
        height: 18,
        width: 18
    },
    clearContainer: {
        marginLeft: 10,
        marginRight: 10
    }
})
export default SearchBar