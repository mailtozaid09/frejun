import React, {useState, useEffect} from 'react'
import { Text, View, StyleSheet, FlatList, TouchableOpacity, TextInput, Image, Dimensions,  } from 'react-native'
import { useNavigation } from '@react-navigation/native';
import Colors from '../../utils/Colors';

import LottieView from 'lottie-react-native';

const {width} = Dimensions.get('window');

const DataEmpty = (props) => {

    const navigation = useNavigation();
    const [data, setData] = useState([]);
    const [loader, setLoader] = useState(true);

    useEffect(() => {
        console.log("DataEmpty---");
    }, []);


    return (
        <View style={styles.mainContainer} >
            <View style={styles.container} >
                <View style={styles.lottie} >
                    <LottieView
                        autoPlay
                        loop
                        style={{height: 220,}}
                        source={require('../../../assets/data-not-found.json')}
                    />
                    
                    <Text style={styles.title} >Ooopss!</Text>
                    <Text style={styles.subTitle} >No Data Found</Text>
                </View>
               
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        width: width,
        alignItems: 'center',
        justifyContent: 'center',
        //backgroundColor: Colors.WHITE
    },
    container: {
        height: 300,
        width: width,
        alignItems: 'center',
    },
    image: {
        height: 200,
        width: 200,
        borderRadius: 20
    },
    lottie: {
        position: 'absolute',
        width: '100%', 
        alignItems: 'center', 
        top: -20,
    },
    title: {
        fontSize: 20,
        lineHeight: 24,
        marginTop: 40,
        color: Colors.BLACK,
        fontWeight: 'bold',
    },
    subTitle: {
        fontSize: 16,
        lineHeight: 24,
        color: Colors.GRAY,
    },
})
export default DataEmpty