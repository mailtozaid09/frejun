const baseUrl = 'https://aboutreact.herokuapp.com/getpost.php'

// get post data
export function getApiData(offset) {
    return(
        fetch(
            baseUrl+`?offset=${offset}`,
            {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                },
            }
        )
        .then(res => res.json())
    );
}
