import React, {useState, useEffect} from 'react'
import {Text, View, Dimensions, StyleSheet, Image, FlatList , TouchableOpacity, ActivityIndicator } from 'react-native';
import Colors from '../utils/Colors';

import { useNavigation } from '@react-navigation/native';

const {width} = Dimensions.get('window');


const HomeScreen = () => {
    const navigation = useNavigation();

    useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
            console.log("Bridge Screen");
        });
        return unsubscribe;
    }, [navigation]);
      


    return (
        <View style={styles.container} >
            <Text style={styles.textStyle} >Bridge Screen</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        //backgroundColor: Colors.WHITE
    },
    textStyle: {
        fontSize: 24, 
        color: Colors.BLACK
    }

})


export default HomeScreen