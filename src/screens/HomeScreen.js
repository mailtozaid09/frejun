import React, {useState, useEffect} from 'react'
import {Text, View, Dimensions, StyleSheet, Image, FlatList , TouchableOpacity, ActivityIndicator } from 'react-native';
import Colors from '../utils/Colors';

import {useDispatch, useSelector} from 'react-redux';
import { useNavigation } from '@react-navigation/native';
import { icons } from '../utils/Icons';
import { setApiData } from '../redux';
import { getApiData } from '../api/api';


const {width} = Dimensions.get('window');


const HomeScreen = () => {
    const navigation = useNavigation();
    const dispatch = useDispatch();
    const apiData = useSelector(state => state.HomeReducer.apiDetails);

    const [loading, setLoading] = useState(true);
    const [isFetching, setIsFetching] = useState(false);
    const [dataSource, setDataSource] = useState([]);
    const [offset, setOffset] = useState(1);

    useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
            console.log("Home Screen");
            onRefresh()
        });
        return unsubscribe;
    }, [navigation]);
      
    const shuffle = (sourceArray) => {
        for (var i = 0; i < sourceArray.length - 1; i++) {
            var j = i + Math.floor(Math.random() * (sourceArray.length - i));

            var temp = sourceArray[j];
            sourceArray[j] = sourceArray[i];
            sourceArray[i] = temp;
        }
        return sourceArray;
    }


    const getData = () => {
        console.log('getData');
        setLoading(true);


        getApiData(offset)
        .then((responseJson) => {
            setOffset(offset + 1);
            setDataSource([...dataSource, ...responseJson.results]);
            setLoading(false);
            setIsFetching(false)
            dispatch(setApiData([...dataSource, ...responseJson.results]))
        })
        .catch((error) => {
            console.log("error >> ", error);
        })
    };

    const onRefresh = () => {
        console.log("onRefresh");
        setLoading(true);
        getApiData(1)
        .then((responseJson) => {
            setOffset(offset + 1);
            
            var result = shuffle(responseJson.results);
            
            setDataSource(result);
            setLoading(false);
            setIsFetching(false)
            dispatch(setApiData(result))
        })
        .catch((error) => {
            console.log("error >> ", error);
        })
    }

     
    const renderFooter = () => {
        return (
        <View style={{}} >
            {loading ? (
            <ActivityIndicator
                size='large' 
                color={Colors.BLACK}
                style={{margin: 10}}
            />
            ) : 
            <TouchableOpacity onPress={getData} style={{  
                shadowColor: "#000",
                shadowOffset: {
                    width: 0,
                    height: 5,
                },
                shadowOpacity: 0.34,
                shadowRadius: 6.27,

                elevation: 10,
                height: 50, alignItems: 'center', 
                flexDirection: 'row', 
                backgroundColor: Colors.WHITE, 
                margin: 15, 
                borderRadius: 10,
                paddingLeft: 15,
                paddingRight: 15,
            }} >
                <Image
                    source={icons.seemore}
                    style={styles.seemoreIcon}
                />
                <Text style={{fontSize: 18, color: Colors.BLACK}} >VIEW MORE DATA</Text>
            </TouchableOpacity>
            }
            
        </View>
        );
    };
     
    const renderItem = ({item}) => {
        return (
            <View style={styles.item}>
                <View style={{flex: 1,}} >
                    <Text style={styles.title}>{(item.title).toUpperCase()}</Text>
                </View>
                <View style={{}} >
                    <Image
                        source={{uri: 'https://images.unsplash.com/photo-1618424181497-157f25b6ddd5?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8Mnx8bGFwdG9wJTIwY29tcHV0ZXJ8ZW58MHx8MHx8&w=1000&q=80'}}
                        style={styles.image}
                    />
                </View>
            </View>
        );
    };

    return (
        <View style={styles.screenContainer} >
            <FlatList
                data={apiData}
                keyExtractor={(item, index) => index.toString()}
                enableEmptySections={true}
                renderItem={renderItem}
                ListFooterComponent={renderFooter}
                onRefresh={() => onRefresh()}
  	            refreshing={isFetching}
            />
        </View>
    )
}

const styles = StyleSheet.create({
    screenContainer: {
        flex: 1,
        backgroundColor: Colors.WHITE
    },
    footer: {
        padding: 10,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
    },
    title: {
        fontSize: 20,
        fontWeight: 'bold',
        color: Colors.BLACK,
    },
    loadMoreBtn: {
        padding: 10,
        backgroundColor: '#800000',
        borderRadius: 4,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    image: {
        height: 100, 
        width: 100, 
        marginLeft: 10, 
        borderRadius: 10,
    },
    btnText: {
        color: Colors.WHITE,
        fontSize: 15,
        textAlign: 'center',
    },
    seemoreIcon: {
        height: 28,
        width: 28,
        resizeMode: 'contain',
        marginRight: 10,
    },
    item: {
        backgroundColor: Colors.WHITE,
        flexDirection: 'row', 
        justifyContent: 'space-between',
        padding: 10,
        marginVertical: 8,
        marginHorizontal: 16,
        borderRadius: 10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 5,
        },
        shadowOpacity: 0.34,
        shadowRadius: 6.27,
        elevation: 10,
    },
})


export default HomeScreen