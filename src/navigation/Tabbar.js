import React,{useEffect, useState} from 'react'
import { Text, View, StyleSheet, ScrollView, TouchableOpacity, Image, LogBox,  } from 'react-native'

import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

import HomeScreen from '../screens/HomeScreen';
import FilterScreen from '../screens/FilterScreen';
import BridgeScreen from '../screens/BridgeScreen';

import Colors from '../utils/Colors';
import { icons } from '../utils/Icons';

const Tab = createBottomTabNavigator();

export default function Tabbar() {

    
    useEffect(() => {
        //console.log("Tabbar");
    }, []);

    return (
        <Tab.Navigator
            screenOptions={({ route }) => ({
                tabBarStyle: {height: 60, },
                tabBarIcon: ({ focused, color, size }) => {
                let iconName, tabName;
    
                if (route.name === 'Home') {
                    tabName= 'Home'
                    iconName = focused
                    ? icons.homeActive
                    : icons.home
                } else if (route.name === 'Filter') {
                    tabName= 'Filter'
                    iconName = focused
                    ? icons.filterActive
                    : icons.filter
                } else if (route.name === 'Bridge') {
                    tabName= 'Bridge'
                    iconName = focused
                    ? icons.bridgeActive
                    : icons.bridge
                } 
                
                return(
                    <View style={{alignItems: 'center'}} >
                        <Image source={iconName} style={{height: 28, width: 28, resizeMode: 'contain'}}/>
                        <Text style={{fontSize: 12, fontWeight: 'bold', color: focused ? Colors.BLACK : Colors.LIGHT_GRAY}} >{tabName}</Text>
                    </View>
                );
                },
                tabBarActiveTintColor: 'tomato',
                tabBarInactiveTintColor: 'gray',
            })}
        >
            <Tab.Screen
                name="Home"
                component={HomeScreen}
                listeners={({ navigation, route }) => ({
                    tabPress: (e) => {
                      console.log("---Home---");
                      e.preventDefault();
                      navigation.navigate('Home')
                    },
                  })}
                options={{
                    headerShown: true,
                    headerTitle: 'Home',
                    tabBarShowLabel: false,
                    headerTitleStyle: {fontSize: 20, fontWeight: 'bold'},
                    headerStyle: {backgroundColor: '#fff', elevation:0 }
                }}
            />
            <Tab.Screen
                name="Filter"
                component={FilterScreen}
                listeners={({ navigation, route }) => ({
                    tabPress: (e) => {
                      console.log("---FilterScreen---");
                      e.preventDefault();
                      navigation.navigate('Filter')
                    },
                  })}
                options={{
                    headerShown: true,
                    headerTitle: 'Filter',
                    tabBarShowLabel: false,
                    headerTitleStyle: {fontSize: 20, fontWeight: 'bold'},
                    headerStyle: { backgroundColor: '#fff', elevation:0 }
                }}
            />
            <Tab.Screen
                name="Bridge"
                component={BridgeScreen}
                listeners={({ navigation, route }) => ({
                    tabPress: (e) => {
                      console.log("---BridgeScreen---");
                      e.preventDefault();
                      navigation.navigate('Bridge')
                    },
                  })}
                options={{
                    headerShown: true,
                    headerTitle: 'Bridge',
                    tabBarShowLabel: false,
                    headerTitleStyle: {fontSize: 20, fontWeight: 'bold'},
                    headerStyle: { backgroundColor: '#fff', elevation:0 }
                }}
            />
        </Tab.Navigator>
    );
}