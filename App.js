import 'react-native-gesture-handler';
import React,{useEffect} from 'react';
import {LogBox} from 'react-native';
import Tabbar from './src/navigation/Tabbar.js';
import { NavigationContainer } from '@react-navigation/native';
import { Provider } from 'react-redux';
import Store from './src/redux/store.js';


LogBox.ignoreAllLogs(true);

const App = () => {

    useEffect(() => {
    
    }, [])
	
	return (
		<Provider store={Store}>
			<NavigationContainer>
				<Tabbar/>
			</NavigationContainer>
		</Provider>
	)
}

export default App